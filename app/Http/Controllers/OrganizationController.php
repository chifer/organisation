<?php

namespace App\Http\Controllers;

use App\Organization;
use App\Http\Services\OrganisationService;
use App\Http\Requests\OrganisationRequest;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param OrganisationRequest $request
     * @param OrganisationService $organisationService
     * @return array
     */
    public function index(
        OrganisationRequest $request,
        OrganisationService $organisationService
    ): array {
        $fields = $request->validated();
        $organisations = Organization::all();
        return $organisationService->getSortedOrganisation($organisations, $fields);
    }
}
