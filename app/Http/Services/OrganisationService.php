<?php

namespace App\Http\Services;

class OrganisationService
{
    /**
     * @param $organisations
     * @param $point
     * @return array
     */
    public function getSortedOrganisation($organisations, $point): array
    {
        $result = [];
        foreach ($organisations as $organisation) {
            $distance = $this->getPointFromOrganisation($point, $organisation['latitude'], $organisation['longitude']);
            $tmp['name'] = $organisation['name'];
            $tmp['address'] = $organisation['address'];
            $tmp['working_time'] = json_decode($organisation['working_time']);
            $tmp['distance'] = $distance;
            $result[] = $tmp;
        }

        return $this->sortByDistance($result);
    }

    /**
     * @param $point
     * @param $lat
     * @param $long
     * @return float
     */
    private function getPointFromOrganisation($point, $lat, $long): float
    {
        $getPoint  = new GeoPointService($lat, $long);
        return $getPoint->distanceTo($point);
    }

    /**
     * @param $organisations
     * @return array
     */
    private function sortByDistance($organisations): array
    {
        $collection = collect($organisations);
        $sorted = $collection->sortBy('distance');
        return $sorted->values()->all();
    }
}
