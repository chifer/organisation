<?php

namespace App\Http\Services;

class GeoPointService
{
    public $lat;
    public $long;

    public function __construct($latitude, $longitude)
    {
        $this->lat = $latitude;
        $this->long = $longitude;
    }

    public function distanceTo($point): float
    {
        $degrees = rad2deg(acos((sin(deg2rad($this->lat)) * sin(deg2rad($point['latitude']))) +
            (cos(deg2rad($this->lat)) * cos(deg2rad($point['latitude'])) * cos(deg2rad($this->long - $point['longitude'])))));

        $distance = $degrees * 111.13384 * 1000;

        return round($distance, 2);
    }
}
