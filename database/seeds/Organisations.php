<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Organisations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = '{"mon": "10:00 - 22:00","tue": "10:00 - 22:00","wed": "10:00 - 22:00",
        "thu": "10:00 - 22:00","fri": "10:00 - 22:00","sat": "10:00 - 22:00","sun": "10:00 - 22:00",}';
        $json = json_encode(str_replace(' ', '', $json));

        if (DB::table('organizations')->count() == 0) {
            for ($i = 1; $i < 51; $i++) {
                DB::table('organizations')->insert(
                    [
                        'id' => (string)Str::uuid(),
                        'name' => 'Organisation - ' . $i,
                        'address' => 'Russia, Moscow city, Domodedovskaya str.' . rand(1, 125),
                        'latitude' => '55.' . rand(600000, 757921),
                        'longitude' => '37.' . rand(600000, 757921),
                        'working_time' => $json,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
            }
        } else {
            echo "\e[31mTable is not empty, therefore NOT ";
        }
    }
}
