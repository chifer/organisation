install:
	 composer install && npm install && php artisan migrate && php artisan db:seed

run:
	php artisan serve

reinstall:
	rm -rf vendor && rm -rf node_modules
