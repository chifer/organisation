
## How to install

Before start tha app, need to install

- php 7.2
- MySQL
- composer
- npm
- Make

### After installation:

copy .env.exmaple file:
```
cp .env.example .env
```
and update it and run:
```
make install
```

## How to use

send post request to:
http://127.0.0.1:8000/api/organisation

with latitude and longitude data and wait json response

